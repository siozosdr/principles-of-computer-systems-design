# README #

This repo contains the assignments and the exam taken in the Principles for Computer System Design (PCSD) course in the MSc. in Computer Science program of DIKU. All the implementations are in Java.

## Assignment 3 ##

This assignment targets the following learning goals:

 - Discuss basic concepts in recovery for durability of a two-level memory
under a restricted set of fail-stop failures.

 - Explain in detail the functioning of the ARIES algorithm in a recovery
scenario.

 - Implement workload generation procedures in an experimental framework.
 
 - Design and conduct an experiment to measure performance of a system.
 
## Assignment 4 ##
 
This assignment targets the following learning goals:

 - Explain the design of communication abstractions, discussing dierences
between asynchronous and synchronous as well as persistent and transient
variants.

 - Perform simple system reliability calculations, while clearly stating under-
lying assumptions.

 - Implement a simple synchronous replication protocol while ensuring a level
of consistency of operations observed by clients.

 - Discuss multiple aspects of the implementation of replication protocols,
including update and read processing, load balancing, scalability, and han-
dling of failures.

## Exam

We attempt to touch on many of the learning goals of PCSD. Recall that the learning goals of PCSD are:

 - (LG1) Describe the design of transactional and distributed systems.
 
 - (LG2) Explain how to enforce modularity through a client-service abstraction.
 
 - (LG3) Discuss design alternatives for a computer system, identifying system properties as well as mechanisms for improving performance.
 
 - (LG4) Analyze protocols for concurrency control and recovery, as well as for distribution and replication.
 
 - (LG5) Implement systems that include mechanisms for modularity, atomicity, and fault tolerance.
 
 - (LG6) Structure and conduct experiments to evaluate a system's performance.
 
 - (LG7) Explain techniques for large-scale data processing.
 
 - (LG8) Apply principles of large-scale data processing to concrete problems