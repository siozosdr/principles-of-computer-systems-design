/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acertainfarm.business;

import com.acertainfarm.interfaces.FieldStatus;
import com.acertainfarm.interfaces.SensorAggregator;
import com.acertainfarm.utils.AttributeOutOfBoundsException;
import com.acertainfarm.utils.FieldConstants;
import com.acertainfarm.utils.PrecisionFarmingException;
import org.w3c.dom.Attr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.*;
/**
 * Implementation of the SensorAggregator
 * @author sokratis
 */
public class CertainSensorAggregator implements SensorAggregator{
    
    private static CertainSensorAggregator singleInstance;
    // HashMap to hold the all the measurements for a specific field
    HashMap<Integer, List<Measurement>> measurementMap = null;
    long startOfWindow = System.currentTimeMillis();
    int numberOfFields;
    int numberOfSensors;
    long windowSize;
    public FieldStatus fieldStatus;
    private final ReadWriteLock myRWLock = new ReentrantReadWriteLock();
    // takes as a parameter a FieldStatus so that it will be able to call the update function
    public CertainSensorAggregator(int numFields, FieldStatus fs, long ws, int numberOfSensors){
        measurementMap = new HashMap<Integer, List<Measurement>>();
        this.numberOfSensors = numberOfSensors;
        numberOfFields = numFields;
        fieldStatus = fs;
        windowSize = ws;
    }

    @Override
    public void newMeasurements(List<Measurement> measurements)  throws AttributeOutOfBoundsException{
    	startOfWindow = System.currentTimeMillis();
    	long timeFrame = startOfWindow + windowSize;
    	
        for (Measurement m : measurements) {
            if(m.getCurrentHumidity()<0 || m.getCurrentHumidity()>100){
                throw new AttributeOutOfBoundsException(FieldConstants.HUMIDITY +
                       m.getCurrentHumidity() + FieldConstants.INVALID);
            }
            if(m.getCurrentTemperature()<-50 || m.getCurrentTemperature()>50){
                 throw new AttributeOutOfBoundsException(FieldConstants.TEMPERATURE +
                        m.getCurrentTemperature() + FieldConstants.INVALID);
            }
            if(m.getFieldId()<1 || m.getFieldId()>numberOfFields){
                 throw new AttributeOutOfBoundsException(FieldConstants.FIELDID +
                        m.getFieldId() + FieldConstants.INVALID);
            }
            if(m.getSensorId()<1 || m.getSensorId()>numberOfSensors){
                throw new AttributeOutOfBoundsException(FieldConstants.SENSORID +
                m.getSensorId() + FieldConstants.INVALID);
            }
            // if windows has not ended yet
            if(System.currentTimeMillis() < timeFrame){
            	myRWLock.writeLock().lock();
                // if there is already a mapping for the field we 
                // simply add the measurements to it
                if(measurementMap.get(m.getFieldId()) != null){
                    List<Measurement> temp = measurementMap.get(m.getFieldId());
                    temp.add(m);
                    measurementMap.put(m.getFieldId(), temp);
                }else{// if there is no mapping for the field we create a 
                      // new one
                    List<Measurement> temp = new ArrayList<Measurement>();
                    temp.add(m);
                    measurementMap.put(m.getFieldId(), temp);
                }
                myRWLock.writeLock().unlock();
            }else{//if window ended
                // create new batch
                myRWLock.writeLock().lock();
                List<Event> batch = new ArrayList<Event>();
                // update start of window for new window
                startOfWindow = System.currentTimeMillis();
                // calculate avgTemperature and avgHumidity for a specific fieldID
                for (Map.Entry<Integer, List<Measurement>> entry : measurementMap.entrySet()) {
                    List<Measurement> temp = entry.getValue();
                    float avgHum = 0;
                    float avgTem = 0;
                    for(Measurement me : temp){
                        avgHum += me.getCurrentHumidity();
                        avgTem += me.getCurrentTemperature();
                    }
                    avgHum /= temp.size();
                    avgTem /= temp.size();
                    // create a new event and add it to the batch which
                    // will be sent to the FieldStatus service.
                    Event e = new Event(entry.getKey(), (int)avgTem, (int)avgHum);
                    batch.add(e);
                    try {
                        fieldStatus.update(timeFrame, batch);
                    } catch (PrecisionFarmingException e1) {
                        System.out.println("FieldSstatus service is not available for updates");
                    }
                }
                //Clear map after all updates are complete
                measurementMap.clear();
                myRWLock.writeLock().unlock();
            }
        }
    }
}
