/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acertainfarm.business;

import com.acertainfarm.interfaces.FieldStatus;
import com.acertainfarm.utils.AttributeOutOfBoundsException;
import com.acertainfarm.utils.FieldConstants;
import com.acertainfarm.utils.PrecisionFarmingException;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.*;

/**
 *  Implementation of the FieldStatus
 * @author sokratis siozos-drosos
 */
public class CertainFieldStatus implements FieldStatus{
    // hashmap to hold field ids with their states
    private static CertainFieldStatus singleInstance;
    HashMap<Integer, FieldState> fieldMap;
    int numberOfFields;
    private final ReadWriteLock myRWLock = new ReentrantReadWriteLock();

    
    public CertainFieldStatus(int numFields){
        fieldMap = new HashMap<Integer, FieldState>();
        numberOfFields = numFields;
    }

    @Override
    public void update(long timePeriod, List<Event> events) 
            throws AttributeOutOfBoundsException, PrecisionFarmingException {
        myRWLock.writeLock().lock();
        for(Event e : events){
            if(e.getAvgHumidity() < 0 || e.getAvgHumidity()>100){
               throw new AttributeOutOfBoundsException(FieldConstants.HUMIDITY +
                       e.getAvgHumidity() + FieldConstants.INVALID);
            }
            if(e.getAvgTemperature() < -50 || e.getAvgTemperature() > 50){
                throw new AttributeOutOfBoundsException(FieldConstants.TEMPERATURE +
                        e.getAvgTemperature() + FieldConstants.INVALID);
            }
            if(e.getFieldId() < 1 || e.getFieldId() > numberOfFields){
                throw new AttributeOutOfBoundsException(FieldConstants.FIELDID +
                        e.getFieldId() + FieldConstants.INVALID);
            }

            FieldState temp;
        	temp = new FieldState(
                     e.getFieldId(),
                     e.getAvgTemperature(),
                     e.getAvgHumidity()
                     );
                         
            fieldMap.put(e.getFieldId(), temp);
            // Write log functionality
            // Source: http://stackoverflow.com/questions/1625234/how-to-append-text-to-an-existing-file-in-java
            try {
                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("log.txt", true)));
                out.println( timePeriod+", "+
                        e.getFieldId()+", "+
                        e.getAvgTemperature()+", "+
                        e.getAvgHumidity());
                out.close();
            } catch (IOException ex) {
                System.out.println("There was a problem while writing");
            }
            
        }

        myRWLock.writeLock().unlock();
    }

    @Override
    public List<FieldState> query(List<Integer> fieldIds) 
            throws AttributeOutOfBoundsException, PrecisionFarmingException {
    	myRWLock.readLock().lock();
        List<FieldState> states = new ArrayList<FieldState>();
        for(Integer i : fieldIds){
            if(i < 1 || i > numberOfFields){
                 throw new AttributeOutOfBoundsException(FieldConstants.FIELDID +
                        i + FieldConstants.INVALID);
            }
            if(fieldMap.get(i)!= null){
                states.add(fieldMap.get(i));
            }

        }
        myRWLock.readLock().unlock();
        return states;
    }
}
