/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acertainfarm.server;

import com.acertainfarm.business.CertainSensorAggregator;
import com.acertainfarm.business.Measurement;
import com.acertainfarm.utils.AttributeOutOfBoundsException;
import com.acertainfarm.utils.FieldMessageTag;
import com.acertainfarm.utils.FieldResponse;
import com.acertainfarm.utils.FieldComponentUtility;
import com.acertainfarm.utils.PrecisionFarmingException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

/**
 *
 * @author sokras
 */
public class SensorAggregatorHTTPMessageHandler extends AbstractHandler{
     private CertainSensorAggregator sensorAggregator = null;
    
    public SensorAggregatorHTTPMessageHandler(CertainSensorAggregator sensorAg){
        sensorAggregator = sensorAg;
    }
    @SuppressWarnings("unchecked")
    public void handle(String target, Request baseRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
        FieldMessageTag messageTag;
        
        String  requestURI;
        FieldResponse fieldResponse = null;
        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        requestURI = request.getRequestURI();
        if (!FieldComponentUtility.isEmpty(requestURI)
				&& requestURI.toLowerCase().startsWith("/stock")) {
			messageTag = FieldComponentUtility.convertURItoMessageTag(requestURI
					.substring(6)); // the request is from store
			// manager, more
			// sophisticated security
			// features could be added
			// here
        } else {
                messageTag = FieldComponentUtility.convertURItoMessageTag(requestURI);
        }
        // the RequestURI before the switch
        if (messageTag == null) {
                System.out.println("Unknown message tag");
        } else {
            switch (messageTag) {
                case NEWMEASUREMENTS:
                    String xml = FieldComponentUtility.extractPOSTDataFromRequest(request);
                    List<Measurement> measurements = (List<Measurement>) FieldComponentUtility
                                    .deserializeXMLStringToObject(xml);

                    fieldResponse = new FieldResponse();
                    try {
                            sensorAggregator.newMeasurements(measurements);
                    } catch (AttributeOutOfBoundsException ex) {
                        Logger.getLogger(FieldStatusHTTPMessageHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    String listMeasurementsxmlString = FieldComponentUtility
                                    .serializeObjectToXMLString(fieldResponse);
                    response.getWriter().println(listMeasurementsxmlString);
                    break;
                default:
                    System.out.println("Unhandled message tag on sensor aggregator");
                    break;
            }
        }
        // Mark the request as handled so that the HTTP response can be sent
        baseRequest.setHandled(true);
    }
}
