/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acertainfarm.server;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

/**
 *
 * @author sokras
 */
public class FieldComponentHTTPServerUtility {
    /**
	 * Creates a server on the port and blocks the calling thread
         * @param port
         * @param handler
         * @return 
	 */
	public static boolean createServer(int port, AbstractHandler handler) {
		Server server = new Server(port);
		if (handler != null) {
			server.setHandler(handler);
		}

		try {
			server.start();
			server.join();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return true;
	}

	/**
	 * Creates a server on the InetAddress and blocks the calling thread
         * @param ipAddress
         * @param port
         * @param handler
         * @return 
	 */
	public static boolean createServer(String ipAddress, int port,
			AbstractHandler handler) {
		InetAddress inetIpAddress;
		InetSocketAddress address;
		Server server;

		if (ipAddress == null)
			return false;

		try {
			inetIpAddress = InetAddress.getByName(ipAddress);
			address = new InetSocketAddress(inetIpAddress, port);
		} catch (UnknownHostException ex) {
			ex.printStackTrace();
			return false;
		}

		server = new Server(address);
		if (handler != null) {
			server.setHandler(handler);
		}

		try {
			server.start();
			server.join();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return true;
	}
}
