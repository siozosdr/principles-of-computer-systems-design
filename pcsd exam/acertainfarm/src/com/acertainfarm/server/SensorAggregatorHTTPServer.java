/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acertainfarm.server;

import com.acertainfarm.business.CertainSensorAggregator;
import com.acertainfarm.interfaces.FieldStatus;
import com.acertainfarm.utils.FieldConstants;

/**
 *
 * @author sokras
 */
public class SensorAggregatorHTTPServer {
     /**
	* @param args
	*/
	public static void main(String[] args) {
		FieldStatus fieldStatus = null;
		CertainSensorAggregator sensorAggregator = new CertainSensorAggregator(300,fieldStatus,5, 300);
		int listen_on_port = 8082;
		SensorAggregatorHTTPMessageHandler handler = new SensorAggregatorHTTPMessageHandler(
				sensorAggregator);
		String server_port_string = System.getProperty(FieldConstants.PROPERTY_KEY_SERVER_PORT);
		if(server_port_string != null) {
			try {
				listen_on_port = Integer.parseInt(server_port_string);
			} catch(NumberFormatException ex) {
				System.err.println(ex);
			}
		}
		if (FieldComponentHTTPServerUtility.createServer(listen_on_port, handler));
		//FieldComponentHTTPServerUtility.createServer(listen_on_portfs, handlerfs);

		
	}
}
