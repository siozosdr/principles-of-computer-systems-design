/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acertainfarm.server;

/**
 *
 * @author sokras
 */
import com.acertainfarm.business.CertainFieldStatus;
import com.acertainfarm.business.Event;
import com.acertainfarm.utils.AttributeOutOfBoundsException;
import com.acertainfarm.utils.FieldDTO;
import com.acertainfarm.utils.FieldMessageTag;
import com.acertainfarm.utils.FieldResponse;
import com.acertainfarm.utils.FieldComponentUtility;
import com.acertainfarm.utils.PrecisionFarmingException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
public class FieldStatusHTTPMessageHandler extends AbstractHandler{

    private CertainFieldStatus myField = null;
    
    public FieldStatusHTTPMessageHandler(CertainFieldStatus field){
        myField = field;
    }
    @SuppressWarnings("unchecked")
    public void handle(String target, Request baseRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
        FieldMessageTag messageTag;
        
        String  requestURI;
        FieldResponse fieldResponse;
        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        requestURI = request.getRequestURI();
        /*if (!FieldComponentUtility.isEmpty(requestURI)
				&& requestURI.toLowerCase().startsWith("/stock")) {
			messageTag = FieldComponentUtility.convertURItoMessageTag(requestURI
					.substring(6)); // the request is from store
			// manager, more
			// sophisticated security
			// features could be added
			// here
        } else {*/
                messageTag = FieldComponentUtility.convertURItoMessageTag(requestURI);
        //}
        // the RequestURI before the switch
        if (messageTag == null) {
                System.out.println("Unknown message tag");
        } else {
            switch (messageTag) {
                case UPDATE:
                    String xml = FieldComponentUtility.extractPOSTDataFromRequest(request);
                    FieldDTO temp = (FieldDTO) FieldComponentUtility
                                    .deserializeXMLStringToObject(xml);
                    long timePeriod = temp.getTimePeriod();
                    List<Event> events = temp.getEvents();
                    fieldResponse = new FieldResponse();
                    try {
                            myField.update(timePeriod, events);
                    } catch (PrecisionFarmingException ex) {
                            fieldResponse.setException(ex);
                    } catch (AttributeOutOfBoundsException ex) {
                        Logger.getLogger(FieldStatusHTTPMessageHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    String listMeasurementsxmlString = FieldComponentUtility
                                    .serializeObjectToXMLString(fieldResponse);
                    response.getWriter().println(listMeasurementsxmlString);
                    break;
                case QUERY:
                    xml = FieldComponentUtility.extractPOSTDataFromRequest(request);
                    List<Integer> fieldIds = (List<Integer>) FieldComponentUtility
                                    .deserializeXMLStringToObject(xml);

                    fieldResponse = new FieldResponse();
                    try {
                            fieldResponse.setList(myField.query(fieldIds));
                    } catch (PrecisionFarmingException ex) {
                            fieldResponse.setException(ex);
                    } catch (AttributeOutOfBoundsException ex) {
                        Logger.getLogger(FieldStatusHTTPMessageHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    listMeasurementsxmlString = FieldComponentUtility
                                    .serializeObjectToXMLString(fieldResponse);
                    response.getWriter().println(listMeasurementsxmlString);
                    break;
                default:
                	System.out.println(messageTag);
                    System.out.println("Unhandled message tag on field Status");
                    break;
            }
        }
        // Mark the request as handled so that the HTTP response can be sent
        baseRequest.setHandled(true);
    }
}
