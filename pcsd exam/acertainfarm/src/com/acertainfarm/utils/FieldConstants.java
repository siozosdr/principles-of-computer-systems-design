/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acertainfarm.utils;

/**
 *
 * @author sokras
 */
public class FieldConstants {

    // Used as error code when converting numbers to integer
    public static final int INVALID_PARAMS = -1;

    // Constants used when creating exception messages
    // When sensorId, fieldId, humidity of temperature is invalid
    public static final String INVALID = " is invalid";
    public static final String HUMIDITY = "The Humidity: ";
    public static final String TEMPERATURE = "The Temperature: ";
    public static final String SENSORID = "The SensorID: ";
    public static final String FIELDID = "The FieldID: ";

    public static final String PROPERTY_KEY_LOCAL_TEST = "localtest";
    public static final String PROPERTY_KEY_SERVER_PORT = "port";
}
