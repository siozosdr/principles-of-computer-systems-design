/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acertainfarm.utils;

import com.acertainfarm.business.Event;
import java.util.List;

/**
 *
 * Object to pass different types of values over RPC. 
 * It is used in FieldStatusHTTPProxy for the update.
 */
public class FieldDTO {
    private long timePeriod;
    private List<Event> events;
    
    
    public FieldDTO(long tp, List<Event> e){
        timePeriod = tp;
        for(Event ev : e){
            events.add(ev);
        }
    }
    public void setTimePeriod(long timePeriod){
        this.timePeriod = timePeriod;
    }
    
    public void setEvents(List<Event> e){
        for(Event ev : e){
            events.add(ev);
        }
    }
    
    public long getTimePeriod(){
        return timePeriod;
    }
    
    public List<Event> getEvents(){
        return events;
    }
}
