/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acertainfarm.utils;

import com.acertainfarm.business.Measurement;
import java.util.List;

/**
 *
 * @author sokras
 */
public class FieldResponse {
    private PrecisionFarmingException exception;
	private List<?> list;

	public PrecisionFarmingException getException() {
		return exception;
	}

	public void setException(PrecisionFarmingException exception) {
		this.exception = exception;
	}

	public FieldResponse(PrecisionFarmingException exception, List<Measurement> list) {
		this.setException(exception);
		this.setList(list);
	}

	public FieldResponse() {
		this.setException(null);
		this.setList(null);
	}

	public List<?> getList() {
		return list;
	}

	public void setList(List<?> list) {
		this.list = list;
	}
}
