package com.acertainfarm.client.workloads;

import com.acertainfarm.interfaces.FieldStatus;
import com.acertainfarm.interfaces.SensorAggregator;

/**
 *
 * WorkloadConfiguration represents the configuration parameters to be used by
 * Workers class for running the workloads
 *
 */
public class WorkloadConfiguration {
    // maximum number of field ids
    private int numOfFields = 300;
    //maximum number of sensor ids
    private int numOfSensors = 150;
    // number of measurements to retrieve from the MeasurementGenerator class
    private int numberOfMeasurementsToGet = 150;
    // number of filedIds to get
    private int numberOfFieldIdsToGet = 300;
    private int warmUpRuns = 100;
    private int numActualRuns = 500;

    private float percentNewMeasurementsInteraction = 80f;
    private float percentQueryInteraction = 20f;
    private MeasurementGenerator measurementGenerator = null;
    private FieldStatus fieldStatus = null;
    private SensorAggregator sensorAggregator = null;

    public WorkloadConfiguration(SensorAggregator sensorAggregator, FieldStatus fieldStatus) throws Exception {
        // Create a new one so that it is not shared
        measurementGenerator = new MeasurementGenerator();
        this.sensorAggregator = sensorAggregator;
        this.fieldStatus = fieldStatus;
    }

    public int getNumOfFields() {
        return numOfFields;
    }

    public void setNumOfFields(int numOfFields) {
        this.numOfFields = numOfFields;
    }

    public int getNumOfSensors() {
        return numOfSensors;
    }

    public void setNumOfSensors(int numOfSensors) {
        this.numOfSensors = numOfSensors;
    }

    public FieldStatus getFieldStatus() {
        return fieldStatus;
    }

    public SensorAggregator getSensorAggregator() {
        return sensorAggregator;
    }

    public void setFieldStatus(FieldStatus fieldStatus) {
        this.fieldStatus = fieldStatus;
    }

    public void setSensorAggregator(SensorAggregator sensorAggregator) {
        this.sensorAggregator = sensorAggregator;
    }

    public float getPercentNewMeasurementsInteraction() {
        return percentNewMeasurementsInteraction;
    }

    public void setPercentNewMeasurementsInteraction(
            float percentNewMeasurementsInteraction) {
        this.percentNewMeasurementsInteraction = percentNewMeasurementsInteraction;
    }

    public float getPercentQueryInteraction() {
        return percentQueryInteraction;
    }

    public void setPercentQueryInteraction(
            float percentQueryInteraction) {
        this.percentQueryInteraction = percentQueryInteraction;
    }

    public int getWarmUpRuns() {
        return warmUpRuns;
    }

    public void setWarmUpRuns(int warmUpRuns) {
        this.warmUpRuns = warmUpRuns;
    }

    public int getNumActualRuns() {
        return numActualRuns;
    }

    public void setNumActualRuns(int numActualRuns) {
        this.numActualRuns = numActualRuns;
    }

    public int getNumberOfMeasurementsToGet() {
        return numberOfMeasurementsToGet;
    }

    public void setNumberOfMeasurementsToGet(int numberOfMeasurementsToGet) {
        this.numberOfMeasurementsToGet = numberOfMeasurementsToGet;
    }

    public int getNumberOfFieldIdsToGet() {
        return numberOfFieldIdsToGet;
    }

    public void setNumberOfFieldIdsToGet(int numberOfFieldIdsToGet) {
        this.numberOfFieldIdsToGet = numberOfFieldIdsToGet;
    }

    public MeasurementGenerator getMeasurementGenerator() {
        return measurementGenerator;
    }

    public void setMeasurementGenerator(MeasurementGenerator measurementGenerator) {
        this.measurementGenerator = measurementGenerator;
    }

}
