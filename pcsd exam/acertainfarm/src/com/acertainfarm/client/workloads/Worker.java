/**
 *
 */
package com.acertainfarm.client.workloads;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

import com.acertainfarm.business.Measurement;
import com.acertainfarm.utils.AttributeOutOfBoundsException;
import com.acertainfarm.utils.PrecisionFarmingException;

/**
 *
 * Worker represents the workload runner which runs the workloads with
 * parameters using WorkloadConfiguration and then reports the results
 *
 */
public class Worker implements Callable<WorkerRunResult> {
    private WorkloadConfiguration configuration = null;
    private int numSuccessfulNewMeasurementsInteraction = 0;
    private int numTotalNewMeasurementsInteraction = 0;

    public Worker(WorkloadConfiguration config) {
        configuration = config;
    }

    /**
     *
     * @return configuration of the worker
     */

    public WorkloadConfiguration getConfiguration(){
        return configuration;
    }
    /**
     * Run the appropriate interaction while trying to maintain the configured
     * distributions
     *
     * Updates the counts of total runs and successful runs for customer
     * interaction
     *
     * @param chooseInteraction
     * @return
     */
    private boolean runInteraction(float chooseInteraction) {
        try {
            if (chooseInteraction < configuration
                    .getPercentQueryInteraction()) {
                runQueryInteraction();
            } else {
                numTotalNewMeasurementsInteraction++;
                runNewMeasurementsInteraction();
                numSuccessfulNewMeasurementsInteraction++;
            }
        }catch (PrecisionFarmingException ex){
            return false;
        } catch (AttributeOutOfBoundsException e) {
            return false;
        }
        return true;
    }

    /**
     * Run the workloads trying to respect the distributions of the interactions
     * and return result in the end
     */
    public WorkerRunResult call() throws Exception {
        int count = 1;
        long startTimeInNanoSecs = 0;
        long endTimeInNanoSecs = 0;
        int successfulInteractions = 0;
        long timeForRunsInNanoSecs = 0;

        Random rand = new Random();
        float chooseInteraction;

        // Perform the warmup runs
        while (count++ <= configuration.getWarmUpRuns()) {
            chooseInteraction = rand.nextFloat() * 100f;
            runInteraction(chooseInteraction);
        }

        count = 1;
        numTotalNewMeasurementsInteraction = 0;
        numSuccessfulNewMeasurementsInteraction = 0;

        // Perform the actual runs
        startTimeInNanoSecs = System.nanoTime();
        while (count++ <= configuration.getNumActualRuns()) {
            chooseInteraction = rand.nextFloat() * 100f;
            if (runInteraction(chooseInteraction)) {
                successfulInteractions++;
            }
        }
        endTimeInNanoSecs = System.nanoTime();
        timeForRunsInNanoSecs += (endTimeInNanoSecs - startTimeInNanoSecs);
        return new WorkerRunResult(successfulInteractions,
                timeForRunsInNanoSecs, configuration.getNumActualRuns(),
                numSuccessfulNewMeasurementsInteraction,
                numTotalNewMeasurementsInteraction);
    }

    /**
     * Runs the new measurements interaction
     *
     * @throws PrecisionFarmingException
     * @throws AttributeOutOfBoundsException
     */
    private void runNewMeasurementsInteraction() throws PrecisionFarmingException, AttributeOutOfBoundsException {

        // Generate measurements
        List<Measurement> measurements = configuration.getMeasurementGenerator().nextMeasurementsList(
                configuration.getNumberOfMeasurementsToGet(),
                configuration.getNumOfFields(),
                configuration.getNumOfSensors()
        );

        // pass measurements to sensor aggregator
        configuration.getSensorAggregator().newMeasurements(measurements);
    }

    /**
     * Runs the query interaction
     *
     * @throws PrecisionFarmingException
     * @throws AttributeOutOfBoundsException
     */
    private void runQueryInteraction() throws PrecisionFarmingException, AttributeOutOfBoundsException {
        // generate list with fieldIds to query
        List<Integer> fieldIds = configuration.getMeasurementGenerator().nextListOfFieldIds(
                configuration.getNumberOfFieldIdsToGet(),
                configuration.getNumOfFields()
        );
        // Query fieldStatus with field ids
        configuration.getFieldStatus().query(fieldIds);
    }

}
