package com.acertainfarm.client.workloads;

import com.acertainfarm.business.Measurement;
import org.junit.Assert;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.*;


public class MeasurementGenerator {
    public MeasurementGenerator() {
    }

    /**
     * Return a list of  randomly selected FieldIds for query()
     * @param num number of ids to return
     * @param numberOfFields number of Fields available
     */
    public List<Integer> nextListOfFieldIds(int num, int numberOfFields) {
        List<Integer> result = new  ArrayList<Integer>();
        RandomUtils r = new RandomUtils();
        for(int i=0;i<num;i++){
            int fieldId = r.nextInt(1, numberOfFields);
            result.add(fieldId);
        }
        return result;
    }

    /**
     * Return a list of randomly generated Measurements for newMeasurements()
     * @param num number of ids to return
     * @param numberOfFields number of Fields available
     * @param numberOfSensors number of sensors available
     */
    public List<Measurement> nextMeasurementsList(int num, int numberOfFields, int numberOfSensors) {
        List<Measurement> result = new  ArrayList<Measurement>();
        RandomUtils r = new RandomUtils();
        int humidity = 0;
        int temperature = 0;
        for(int i=0;i<num;i++){

            result.add(new Measurement(r.nextInt(1,numberOfSensors),
                    r.nextInt(1,numberOfFields),
                    r.nextInt(-50,50),
                    r.nextInt(0,100)));
        }
        return result;
    }

    public class RandomUtils extends Random {

        /**
         * @param min generated value. Can't be > then max
         * @param max generated value
         * @return values in closed range [min, max].
         */
        public int nextInt(int min, int max) {
            Assert.assertFalse("min can't be > then max; values:[" + min + ", " + max + "]", min > max);
            if (min == max) {
                return max;
            }

            return nextInt(max - min + 1) + min;
        }
    }

}
