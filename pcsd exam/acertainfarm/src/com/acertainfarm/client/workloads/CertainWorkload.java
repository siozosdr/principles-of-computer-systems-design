/**
 *
 */
package com.acertainfarm.client.workloads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.acertainfarm.business.CertainFieldStatus;
import com.acertainfarm.business.CertainSensorAggregator;
import com.acertainfarm.client.SensorAggregatorHTTPProxy;
import com.acertainfarm.client.FieldStatusHTTPProxy;
import com.acertainfarm.interfaces.SensorAggregator;
import com.acertainfarm.interfaces.FieldStatus;
import com.acertainfarm.utils.FieldConstants;
import com.acertainfarm.utils.PrecisionFarmingException;

/**
 *
 * CertainWorkload class runs the workloads by different workers concurrently.
 * It configures the environment for the workers using WorkloadConfiguration
 * objects and reports the metrics
 *
 */
public class CertainWorkload {

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        int numConcurrentWorkloadThreads = 10;
        String serverAddress = "http://localhost:8081";
        boolean localTest = true;
        List<WorkerRunResult> workerRunResults = new ArrayList<WorkerRunResult>();
        List<Future<WorkerRunResult>> runResults = new ArrayList<Future<WorkerRunResult>>();
        // Initialize the RPC interfaces if its not a localTest, the variable is
        // over-ridden if the property is set
        String localTestProperty = System
                .getProperty(FieldConstants.PROPERTY_KEY_LOCAL_TEST);
        localTest = (localTestProperty != null) ? Boolean
                .parseBoolean(localTestProperty) : localTest;

        SensorAggregator sensorAggregator = null;
        FieldStatus fieldStatus = null;
        if (localTest) {
            fieldStatus = new CertainFieldStatus(300);
            sensorAggregator = new CertainSensorAggregator(300, fieldStatus, 100, 300);
        } else {
            fieldStatus = new FieldStatusHTTPProxy(serverAddress + "/stock");
            sensorAggregator = new SensorAggregatorHTTPProxy(serverAddress, fieldStatus);
        }
        ExecutorService exec = Executors
                .newFixedThreadPool(numConcurrentWorkloadThreads);

        for (int i = 0; i < numConcurrentWorkloadThreads; i++) {
            WorkloadConfiguration config = new WorkloadConfiguration(sensorAggregator,
                    fieldStatus);
            Worker workerTask = new Worker(config);
            // Keep the futures to wait for the result from the thread
            runResults.add(exec.submit(workerTask));
        }

        // Get the results from the threads using the futures returned
        for (Future<WorkerRunResult> futureRunResult : runResults) {
            WorkerRunResult runResult = futureRunResult.get(); // blocking call
            workerRunResults.add(runResult);
        }

        exec.shutdownNow(); // shutdown the executor

        // Finished initialization, stop the clients if not localTest
        if (!localTest) {
            ((SensorAggregatorHTTPProxy) sensorAggregator).stop();
            ((FieldStatusHTTPProxy) fieldStatus).stop();
        }

        reportMetric(workerRunResults);
    }

    /**
     * Computes the metrics and prints them
     *
     * @param workerRunResults
     */
    public static void reportMetric(List<WorkerRunResult> workerRunResults) {
        double successfulFreqInteractionRuns = 0;
        double totalFreqInteractionRuns = 0;
        double time = 0;
        for (WorkerRunResult result : workerRunResults) {
            successfulFreqInteractionRuns  += result.getSuccessfulFrequentBookStoreInteractionRuns();
            totalFreqInteractionRuns += result.getTotalFrequentBookStoreInteractionRuns();
            time += result.getElapsedTimeInNanoSecs();

        }
        //average seconds elapsed for workers
        double averageSeconds = (time / workerRunResults.size()) / 1000000000.0f; //10^9
        double goodput  = successfulFreqInteractionRuns  / averageSeconds;
        double throughput = totalFreqInteractionRuns / averageSeconds;
        double latency = averageSeconds / (successfulFreqInteractionRuns  / workerRunResults.size());

//		System.out.println(
//				"Workers: "+workerRunResults.size() +
//				", Goodput/Aggregate throughput: " + goodput +
//				", Throughput: " + throughput +
//				", Latency: " + latency);
        System.out.println(
                "" + workerRunResults.size() +
                        "," + goodput +
                        "," + throughput +
                        "," + latency);

    }
}
