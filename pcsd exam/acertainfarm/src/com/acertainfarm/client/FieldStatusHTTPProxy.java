/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acertainfarm.client;

/**
 *
 * @author sokras
 */
import com.acertainfarm.business.Event;
import com.acertainfarm.business.FieldState;
import com.acertainfarm.interfaces.FieldStatus;
import com.acertainfarm.utils.AttributeOutOfBoundsException;
import com.acertainfarm.utils.FieldDTO;
import com.acertainfarm.utils.FieldMessageTag;
import com.acertainfarm.utils.FieldComponentUtility;
import com.acertainfarm.utils.PrecisionFarmingException;

import java.util.List;

import org.eclipse.jetty.client.ContentExchange;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.io.Buffer;
import org.eclipse.jetty.io.ByteArrayBuffer;
import org.eclipse.jetty.util.thread.QueuedThreadPool;


public class FieldStatusHTTPProxy implements FieldStatus{

    
    protected HttpClient client;
    protected String serverAddress;
    
    public FieldStatusHTTPProxy(String serverAddress) throws Exception {
		setServerAddress(serverAddress);
		client = new HttpClient();
		client.setConnectorType(HttpClient.CONNECTOR_SELECT_CHANNEL);
		client.setMaxConnectionsPerAddress(FieldClientConstants.CLIENT_MAX_CONNECTION_ADDRESS); // max
		// concurrent
		// connections
		// to
		// every
		// address
		client.setThreadPool(new QueuedThreadPool(
				FieldClientConstants.CLIENT_MAX_THREADSPOOL_THREADS)); // max
		// threads
		client.setTimeout(FieldClientConstants.CLIENT_MAX_TIMEOUT_MILLISECS); // seconds
		// timeout;
		// if
		// no
		// server
		// reply,
		// the
		// request
		// expires
		client.start();
	}
    
    public String getServerAddress() {
		return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
            this.serverAddress = serverAddress;
    }

    public void update(long timePeriod, List<Event> events){
        ContentExchange exchange = new ContentExchange();
        String urlString = serverAddress + "/" + FieldMessageTag.UPDATE;
        FieldDTO data = new FieldDTO(timePeriod, events);
        String setUpdatexmlString = FieldComponentUtility
                        .serializeObjectToXMLString(data);
        exchange.setMethod("POST");
        exchange.setURL(urlString);
        Buffer requestContent = new ByteArrayBuffer(setUpdatexmlString);
        exchange.setRequestContent(requestContent);

        try {
            FieldComponentUtility.SendAndRecv(this.client, exchange);
        } catch (PrecisionFarmingException e) {
            System.out.println("FieldStatus is unavailable for updates");
        }
    }
    
    public List<FieldState> query(List<Integer> fieldIds)  {
        ContentExchange exchange = new ContentExchange();
        String urlString = serverAddress + "/" + FieldMessageTag.QUERY;

        String listFieldIdsxmlString = FieldComponentUtility
				.serializeObjectToXMLString(fieldIds);
        exchange.setMethod("POST");
        exchange.setURL(urlString);
        Buffer requestContent = new ByteArrayBuffer(listFieldIdsxmlString);
        exchange.setRequestContent(requestContent);
        try {
            return (List<FieldState>) FieldComponentUtility.SendAndRecv(this.client, exchange);
        } catch (PrecisionFarmingException e) {
            System.out.println("FieldStatus is unavailable for queries");
            return null;
        }

    }
    
    public void stop() {
        try {
                client.stop();
        } catch (Exception e) {
                e.printStackTrace();
        }
    }
    
}
