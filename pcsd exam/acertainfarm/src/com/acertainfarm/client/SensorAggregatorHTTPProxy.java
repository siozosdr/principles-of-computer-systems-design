/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acertainfarm.client;

import com.acertainfarm.business.Measurement;
import com.acertainfarm.interfaces.FieldStatus;
import com.acertainfarm.interfaces.SensorAggregator;
import com.acertainfarm.utils.AttributeOutOfBoundsException;
import com.acertainfarm.utils.FieldComponentUtility;
import com.acertainfarm.utils.FieldMessageTag;
import com.acertainfarm.utils.PrecisionFarmingException;

import org.eclipse.jetty.client.ContentExchange;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.io.Buffer;
import org.eclipse.jetty.io.ByteArrayBuffer;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

import java.util.List;

/**
 *
 * @author sokras
 */
public class SensorAggregatorHTTPProxy implements SensorAggregator{

    protected HttpClient client;
    protected String serverAddress;
    public FieldStatus fieldStatus;
    
    public SensorAggregatorHTTPProxy(String serverAddress, FieldStatus fs) throws Exception {
    	fieldStatus = fs;
        setServerAddress(serverAddress);
        client = new HttpClient();
        client.setConnectorType(HttpClient.CONNECTOR_SELECT_CHANNEL);
        client.setMaxConnectionsPerAddress(FieldClientConstants.CLIENT_MAX_CONNECTION_ADDRESS); // max
        // concurrent
        // connections
        // to
        // every
        // address
        client.setThreadPool(new QueuedThreadPool(
                        FieldClientConstants.CLIENT_MAX_THREADSPOOL_THREADS)); // max
        // threads
        client.setTimeout(FieldClientConstants.CLIENT_MAX_TIMEOUT_MILLISECS); // seconds
        // timeout;
        // if
        // no
        // server
        // reply,
        // the
        // request
        // expires
        client.start();
    }
    
    public String getServerAddress() {
		return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
            this.serverAddress = serverAddress;
    }
    
    public void newMeasurements(List<Measurement> measurements) {
        ContentExchange exchange = new ContentExchange();
        String urlString = serverAddress+ "/" + FieldMessageTag.NEWMEASUREMENTS;
        String setMeasurementsxmlString = FieldComponentUtility
                        .serializeObjectToXMLString(measurements);
        exchange.setMethod("POST");
        exchange.setURL(urlString);
        Buffer requestContent = new ByteArrayBuffer(setMeasurementsxmlString);
        exchange.setRequestContent(requestContent);

        try {
            FieldComponentUtility.SendAndRecv(this.client, exchange);
        } catch (PrecisionFarmingException e) {
            System.out.println("Sensor Aggregator is unavailable for new measurements");
        }
    }
    
    public void stop() {
        try {
                client.stop();
        } catch (Exception e) {
                e.printStackTrace();
        }
    }
    
}
