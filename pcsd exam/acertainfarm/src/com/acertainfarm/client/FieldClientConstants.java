/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acertainfarm.client;

/**
 *
 * @author sokras
 */
public final class FieldClientConstants {
    public static final int CLIENT_MAX_CONNECTION_ADDRESS = 200;
	public static final int CLIENT_MAX_THREADSPOOL_THREADS = 250;
	public static final int CLIENT_MAX_TIMEOUT_MILLISECS = 30000;

	public static final String strERR_CLIENT_REQUEST_SENDING = "ERR_CLIENT_REQUEST_SENDING";
	public static final String strERR_CLIENT_REQUEST_EXCEPTION = "ERR_CLIENT_REQUEST_EXCEPTION";
	public static final String strERR_CLIENT_REQUEST_TIMEOUT = "CLIENT_REQUEST_TIMEOUT";
	public static final String strERR_CLIENT_RESPONSE_DECODING = "CLIENT_RESPONSE_DECODING";
	public static final String strERR_CLIENT_UNKNOWN = "CLIENT_UNKNOWN";
}
