/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acertainfarm.client.tests;

import com.acertainfarm.business.CertainFieldStatus;
import com.acertainfarm.business.CertainSensorAggregator;
import com.acertainfarm.business.FieldState;
import com.acertainfarm.business.Measurement;
import com.acertainfarm.client.FieldStatusHTTPProxy;
import com.acertainfarm.client.SensorAggregatorHTTPProxy;
import com.acertainfarm.interfaces.FieldStatus;
import com.acertainfarm.interfaces.SensorAggregator;
import com.acertainfarm.utils.AttributeOutOfBoundsException;
import com.acertainfarm.utils.FieldConstants;
import com.acertainfarm.utils.PrecisionFarmingException;

import java.util.*;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 *
 * @author sokras
 */
public class FieldStatusTest {
    private static final int TEST_NUMFIELDS = 300;
    private static final int TEST_NUMSENSORS = 300;
    private static final long WINDOW_SIZE = 50;
    private static boolean localTest = true;
    private static FieldStatus fieldStatus;
    private static SensorAggregator sensorAggregator;

    @BeforeClass
    public static void setUpBeforeClass() {
            try {
                    String localTestProperty = System
                                    .getProperty(FieldConstants.PROPERTY_KEY_LOCAL_TEST);
                    localTest = (localTestProperty != null) ? Boolean
                                    .parseBoolean(localTestProperty) : localTest;
                    if (localTest) {
                            fieldStatus = new CertainFieldStatus(TEST_NUMFIELDS);
                            sensorAggregator = new CertainSensorAggregator(TEST_NUMFIELDS, fieldStatus, WINDOW_SIZE, TEST_NUMSENSORS);
                    } else {
                            fieldStatus = new FieldStatusHTTPProxy(
                                            "http://localhost:8081");
                            sensorAggregator = new SensorAggregatorHTTPProxy("http://localhost:8082",fieldStatus);
                    }
            } catch (Exception e) {
                    e.printStackTrace();
            }
    }

    /**
     * Tests the functionality of query and newMeasurements with a single thread.
     * Only positive integer are fed to the sensorAggregator for testing purposes
     * but it can handle the full range of the values.
     * @throws AttributeOutOfBoundsException
     * @throws PrecisionFarmingException
     */
    @Test
    public void testMeasurements() throws AttributeOutOfBoundsException, PrecisionFarmingException{
        List<Measurement> measurements = new ArrayList<Measurement>();
        for(int j=0;j<2000;j++){
	        for(int i=1;i<TEST_NUMFIELDS;i++){
                //int temp = r.nextInt(-50,50);
                int temp = i%50;
                //int hum = r.nextInt(0,100);
                int hum = i%100;
                //int fid = r.nextInt(1,TEST_NUMFIELDS);
                int fid = i % TEST_NUMFIELDS;
                //int sid = r.nextInt(1,TEST_NUMSENSORS);
                int sid = i % TEST_NUMSENSORS;
	            measurements.add(new Measurement(sid, fid, temp, hum));
                sensorAggregator.newMeasurements(measurements);

	        }

	       // System.out.println(j);
	        measurements.clear();
        }
        List<Integer> fieldIds = new ArrayList<Integer>();
        for(int i=1;i<TEST_NUMFIELDS+1;i++){
            fieldIds.add(i);
        }
        System.out.println("START QUERYING");
        List<FieldState> states = fieldStatus.query(fieldIds);
        for(FieldState t : states){
            System.out.println(t.getFieldId()+","+t.getTemperature()+","
            +t.getHumidity());
        }
    }

    /**
     * Tests the atomicity of the newMeasurements function with 3 threads.
     */
    @Test
    public void testConcurrentMeasurements(){
        // create clients
        Thread sensor1 = new Thread (new ConcurrentMeasurements());
        Thread sensor2 = new Thread (new ConcurrentMeasurements());
        Thread sensor3 = new Thread (new ConcurrentMeasurements());

        // run threads
        sensor1.start();
        sensor2.start();
        sensor3.start();

        // wait
        try {
            sensor1.join();
            sensor2.join();
            sensor3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
            fail();
        }
        List<Integer> fieldIds = new ArrayList<Integer>();
        for(int i=1;i<TEST_NUMFIELDS;i++){
            fieldIds.add(i);
        }
        System.out.println("START QUERYING");
        List<FieldState> states = null;
        try {
            states = fieldStatus.query(fieldIds);
        } catch (AttributeOutOfBoundsException e) {
            e.printStackTrace();
        } catch (PrecisionFarmingException e) {
            e.printStackTrace();
        }
        for(FieldState t : states){
                System.out.println(t.getFieldId()+","+t.getTemperature()+","
                        +t.getHumidity());
        }
    }

    class ConcurrentMeasurements implements Runnable{

        @Override
        public void run() {
            List<Measurement> measurements = new ArrayList<Measurement>();
            for(int j=0;j<200;j++){
                for(int i=1;i<TEST_NUMFIELDS;i++){
                    int temp = i%50;
                    int hum = i%100;
                    int fid = i % TEST_NUMFIELDS;
                    int sid = i % TEST_NUMSENSORS;
                    measurements.add(new Measurement(sid, fid, temp, hum));
                }
                try {
                    sensorAggregator.newMeasurements(measurements);
                } catch (AttributeOutOfBoundsException e) {
                    System.out.println("Attributes are out of bounds");
                } catch (PrecisionFarmingException e) {
                    System.out.println("Something went wrong");
                }
                measurements.clear();
            }

        }
    }

    @AfterClass
    public static void tearDownAfterClass() throws PrecisionFarmingException {
            if (!localTest) {
                    ((SensorAggregatorHTTPProxy) sensorAggregator).stop();
                    ((FieldStatusHTTPProxy) fieldStatus).stop();
            }
    }
}
